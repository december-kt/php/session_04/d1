<!-- Ideal place to connect php files -->
<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="widtd=device-width initial-scale=1">

		<title>s04: Access Modifiers and Encapsulation</title>
	</head>
	<body>

		<h1>Objects from Variable</h1>
		<p>
			<?php echo $buildingObj->name; ?>
		</p>
		
		<h1>Objects from Classes</h1>
		<p>
			<?php echo var_dump($building); ?>
		</p>
		<p>
			<!-- Method -->
			<?php echo $building->printName() ?>
		</p>
			
		<h1>Objects from Inheritance and Polymorphism</h1>
		<p>
			<?php echo $condominium->printName() ?>
		</p>

		<!-- Session 04 -->
		<h1>Access Modifiers</h1>

		<h2>Building Variables</h2>
		<p>
			<?php //echo $building->name; ?>
		</p>

		<h2>Condominium Variables</h2>
		<p>
			<?php //echo $condominium->name; ?>
		</p>

		<h1>Encapsulation</h1>
		<p>The name of the condominium is <?php echo $condominium->getName(); ?>
		</p>

		<p>
			<?php $condominium->setName('Enzo Tower'); ?>
		</p>

		<p>The name of the condo has been changed to <?php echo $condominium->getName(); ?></p>

	</body>
</html>